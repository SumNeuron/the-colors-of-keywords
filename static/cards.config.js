import scryios from 'scryios'
import {manaPipToHTML} from '@/static/pips.js'
import keywords from '@/static/keywords'
import {identity} from 'frxt'

export const fieldOrder = [
  'keyword', 'W', 'U', 'B', 'R', 'G'
]

export const fieldSynonymMap = {
  'mana cost': 'mana_cost'
}

export const fieldRenderFunctions = {
  prices: (id, field, record) => {
    if (!record[field].usd) return '–'
    return `$${record[field].usd}`
  },
  scryfall_uri: (id, field, record) => `<a target="_blank" href="${record[field]}">view on scryfall</a>`,
  uri: (id, field, record) => `<a target="_blank" href="${record[field]}">view on scryfall</a>`,
  mana_cost: (id, field, record) => {
    let mc = record[field] === undefined ? '' : record[field]
    if (mc === '') return ''
    let pips = scryios.parseManaCost(record[field])
    return pips.map(pip=>manaPipToHTML(pip)).join('')
  },
  colors: (id, field, record) => {
    let mc = record[field] === undefined ? [] : record[field]
    if (mc.length === 0) return ''
    let mana = mc.map(e=>`{${e}}`).join('')
    let pips = scryios.parseManaCost(mana)
    if (pips.length === 0) return ''
    return pips.map(pip=>manaPipToHTML(pip)).join('')
  },
  keywords: (i, f, r) => {
    return r[f].join(', ')
  }

}

export const fieldSortByFunctions = {
  prices: (id, field, record) => isNaN(record[field].usd) ? 0 : Number(record[field].usd),
  mana_cost: (id, field, record) => {
    if (record[field] === undefined) return 0
    let pips = scryios.parseManaCost(record[field])
    let c = 0
    pips.forEach((pip,i)=>{
      let p = scryios.stripPip(pip)
      if (isNaN(p)) c += 1
      else c += Number(p)
    })
    return c
    // return
  },
  colors: (id, field, record) => {
    let total = 0
    if (record[field] === undefined) return total
    record[field].forEach(c=>total+=(scryios.MONO_COLOR_KEYS.indexOf(c)+1))
    return total
  },
  keywords: (id, field, record) => {
    let a = record[field]
    let c = 0
    a.forEach((e, i)=>{
      c += keywords.indexOf(e)
    })
    return c
  }
}

export const fieldFilterFunctions = {
  prices: (id, field, record) => {
    return isNaN(record[field].usd) ? 0 : Number(record[field].usd)
  },
  mana_cost: (id, field, record) => {
    if (record[field] === undefined) return 0
    let pips = scryios.parseManaCost(record[field])
    return pips.map((pip, i)=>scryios.stripPip(pip))
    // let c = 0
    // pips.forEach((pip,i)=>{
    //   let p = scryios.stripPip(pip)
    //   if (isNaN(p)) c += 1
    //   else c += Number(p)
    // })
    // return c
    // return
  },

}







const keywordArrayToString = (id, field, record) => {
  let keywords = record[field]
  return keywords.join(', ')
}

const keywordArrayToNKeywords = (id, field, record) => {
  let keywords = record[field]
  return keywords.length
}

const keywordArrayToNKeywordsString = (id, field, record) => {
  let n = keywordArrayToNKeywords(id, field, record)
  return `${n} keywords`
}

const makeKeywordFunction = (params) => {
  let name = params.name
  if (name === 'keywordArrayToString') return keywordArrayToString
  if (name === 'keywordArrayToNKeywords') return keywordArrayToNKeywords
  if (name === 'keywordArrayToNKeywordsString') return keywordArrayToNKeywordsString
  return identity
}

const makePricesFunction = params => {
  let unit = 'usd'
  if (params.unit && ['eur', 'tix', 'usd'].indexOf(params.unit) > -1) {
    unit = params.unit
    return (id, field, record) => {
      let val = record[field][unit]
      val = isNaN(val) ? 0 : Number(val)
      if (params.showUnit === true) {
        let symb = '$'
        if (unit === 'eur') symb = '€'
        if (unit === 'tix') symb = '¤'
        return `${symb}${val}`
      }
      return val
    }
  }

}

// then we provide a function lookup
export const functionLookup = {
  makeKeywordFunction,
  makePricesFunction
}

// lastly we define the config
export const extractorConfig = {
  prices: {
    usd: {
      renderConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'usd', showUnit: true}
      },
      filterConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'usd', showUnit: false}
      },
      sortByConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'usd', showUnit: false}
      },
    },
    eur: {
      renderConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'eur', showUnit: true}
      },
      filterConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'eur', showUnit: false}
      },
      sortByConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'eur', showUnit: false}
      },
    },
    tix: {
      renderConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'tix', showUnit: true}
      },
      filterConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'tix', showUnit: false}
      },
      sortByConfig: {
        functionName: 'makePricesFunction',
        params: {unit:'tix', showUnit: false}
      },
    }
  },
  keywords: { // <--- field which is configurable
    showKeywords: { // <-- option name
      renderConfig: { //<--- how it should render
        functionName: 'makeKeywordFunction',
        params: { name: 'keywordArrayToString' }
      },
      filterConfig: { //<--- how it should filter
        functionName: 'makeKeywordFunction',
        params: { name: 'identity' }
      },
      sortByConfig: { //<--- how it should sort
        functionName: 'makeKeywordFunction',
        params: { name: 'keywordArrayToNKeywords' }
      }
    },

    showNKeywords: {
      renderConfig: {
        functionName: 'makeKeywordFunction',
        params: { name: 'keywordArrayToNKeywordsString' }
      },
      filterConfig: {
        functionName: 'makeKeywordFunction',
        params: { name: 'keywordArrayToNKeywords' }
      },
      sortByConfig: {
        functionName: 'makeKeywordFunction',
        params: { name: 'keywordArrayToNKeywords' }
      }
    }
  }
}
